"""bumps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.urls import path

from main.views import login_page
from .views import *

urlpatterns = [
	path('', main_page, name='bumps'),
    path('formulario/', formulario_page, name='formulario'),
    path('reportados/', reportados_page, name='reportados'),
    path('reportados/<int:folio>/', folio_reportado_page, name='folio-reportado'),
    # path('reparados/', reparados_page, name='reparados'),
    path('preguntas/', preguntas_page, name='preguntas'),

    path('login/', login_page, name="login"),
	# Apps Urls
    path('main/', include('main.urls')),

    path('admin/', admin.site.urls),
]

if settings.DEBUG:

    urlpatterns = urlpatterns + static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT)

    urlpatterns = urlpatterns + static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)