from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, reverse
from django.shortcuts import reverse
from django.contrib import messages
from main.models import Reporte
from main.forms import ReporteForm
from random import randint

def main_page(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect (reverse('dashboard'))
    else:
    	context = {
    	}
    	return render(request, "landings/landing.html",context)

def random_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def formulario_page(request):
    if request.method == 'POST':
        if 'addReporte' in request.POST:
            form = ReporteForm(request.POST or None, request.FILES or None)

            if form.is_valid():
                print("form")
                first_name = request.POST.get('first_name')
                last_name = request.POST.get('last_name')
                email = request.POST.get('email')
                calle_principal = request.POST.get('calle_principal')
                calle_referencia = request.POST.get('calle_referencia')
                colonia = request.POST.get('colonia')
                municipio = request.POST.get('municipio')
                observaciones = request.POST.get('observaciones')
                image = form.cleaned_data.get('image')
                folio = random_digits(6)

                reporte = Reporte.objects.create(folio=folio,first_name=first_name,last_name=last_name,email=email,calle_principal=calle_principal,calle_referencia=calle_referencia,colonia=colonia,municipio=municipio,observaciones=observaciones)
                try:
                    reporte.image = image
                    reporte.save()
                except Exception as e:
                    print(e)

                print("guardado")
                context = {
                    'reporte': reporte, 
                }
                return render(request, "landings/completado.html",context)
            else:
                print("error")

    context = {
    }
    return render(request, "landings/formulario.html",context)

def reportados_page(request):
    context = {
    }
    return render(request, "landings/reportados.html",context)

def folio_reportado_page(request,folio):
    reporte = Reporte.objects.get(folio=folio)
    context = {
        'reporte': reporte,
    }
    return render(request, "landings/reporte.html",context)

def preguntas_page(request):
    context = {
    }
    return render(request, "landings/preguntas.html",context)
