# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

class 	ProfileAdmin(admin.ModelAdmin):
	list_display = ['__str__','id']
	class Meta:
		model = Profile

admin.site.register(Profile, ProfileAdmin)

class 	ReporteAdmin(admin.ModelAdmin):
	list_display = ['__str__','id']
	class Meta:
		model = Reporte

admin.site.register(Reporte, ReporteAdmin)