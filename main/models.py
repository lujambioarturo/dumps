from __future__ import unicode_literals
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User
from django.core.cache import cache
from django.utils import timezone
from django.contrib import auth
from django.db.models import Q
from datetime import timedelta
from django.db import models
import datetime
import random
import os


def get_filename_ext(filepath):
	base_name = os.path.basename(filepath)
	name, ext = os.path.splitext(base_name)
	return name, ext

def upload_image_path(instance, filename):
	new_filename = random.randint(1,3910209312)
	name, ext = get_filename_ext(filename)
	final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)

	return "main/img/{final_filename}".format(
			new_filename=new_filename,
			final_filename=final_filename
			)

class ProfileManager(models.Manager):
	def get_by_id(self, id):
		qs= self.get_queryset().filter(id=id)#Solicitud.objects
		if qs.count() == 1:
			return qs.first()
		return None

class TouchStampModel(models.Model):
	created 				= models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
	last_modified			= models.DateTimeField(auto_now=True, editable=False, null=False, blank=False)

	class Meta:
		abstract = True

class Profile(models.Model):
	user 							= models.OneToOneField(User, related_name = 'Profile',on_delete=models.PROTECT)
	image							= models.ImageField(upload_to=upload_image_path, null=True, blank=True)
	phone							= models.CharField(max_length=120,null=True, blank=True)
	about 							= models.TextField(blank=True, null=True)

	objects = ProfileManager()

	def get_absolute_url(self):
		return "/user/{pk}/".format(pk=self.pk)

	def __str__(self):
		return self.user.username

	def __unicode__(self):
		return self.user.username

class Reporte(models.Model):
	folio							= models.IntegerField(null=False, blank=False)
	first_name						= models.CharField(max_length=120,null=False, blank=False)
	last_name						= models.CharField(max_length=120,null=False, blank=False)
	email							= models.CharField(max_length=120,null=False, blank=False)
	calle_principal					= models.CharField(max_length=120,null=False, blank=False)
	calle_referencia				= models.CharField(max_length=120,null=False, blank=False)
	colonia							= models.CharField(max_length=120,null=False, blank=False)
	municipio						= models.CharField(max_length=120,null=False, blank=False)
	observaciones					= models.CharField(max_length=120,null=True, blank=True)
	image							= models.ImageField(upload_to=upload_image_path, null=True, blank=True)
	status							= models.BooleanField(default=False)

	created 						= models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
	last_modified					= models.DateTimeField(auto_now=True, editable=False, null=False, blank=False)


	def __str__(self):
		return str(self.folio)

	def __unicode__(self):
		return str(self.folio)