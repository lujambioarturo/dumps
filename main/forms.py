from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.models import User
from django import forms
from .models import Reporte

User = get_user_model()

class LoginForm(forms.Form):
	username = forms.CharField(required=True)
	password = forms.CharField(widget=forms.PasswordInput, required=True)

	def clean_username(self):
		username = self.cleaned_data.get('username')
		qs = User.objects.filter(username=username)
		if qs.exists():
			return username
		else:
			raise forms.ValidationError("El Usuario ingresado no existe")

	def clean(self):
		username = self.cleaned_data.get('username')
		password = self.cleaned_data.get('password')
		user = authenticate(username=username, password=password)
		qss = User.objects.filter(username=username)
		if not user:
			if qss.exists():
				raise forms.ValidationError("Password is Incorret")
			else:
				return self.cleaned_data

class EmployeeForm(forms.Form):
    id_user = forms.CharField(required=False)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(required=True)
    password = forms.CharField(required=True)

    def clean(self):
        data = self.cleaned_data
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

    def clean_email(self):
        """Validate email isnt register already"""
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)

        if qs.exists():
            raise forms.ValidationError(email+" is already register")
        return email

class EmployeeEditForm(forms.Form):
    id_user = forms.CharField(required=False)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(required=True)
    password = forms.CharField(required=False)

    def clean(self):
        data = self.cleaned_data
        id_user = self.cleaned_data.get('id_user')
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

class EmployeeDeleteForm(forms.Form):
    id_user = forms.CharField(required=True)

    def clean(self):
        data = self.cleaned_data
        id_user = self.cleaned_data.get('id_user')


class ReporteForm(forms.Form):
    def file_size(value):
        limit = 2.5 * 1024 * 1024
        if value.size > limit:
            raise ValidationError('Imagen demasiado grande. Tu imagen no puede pesar más de 2.5 MiB.')

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(required=True)
    calle_principal = forms.CharField(required=True)
    calle_referencia = forms.CharField(required=True)
    colonia = forms.CharField(required=True)
    municipio = forms.CharField(required=True)
    observaciones = forms.CharField(required=False)
    image = forms.ImageField(validators=[file_size],required=False)

    def clean(self):
        data = self.cleaned_data
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        email = self.cleaned_data.get('email')
        calle_principal = self.cleaned_data.get('calle_principal')
        calle_referencia = self.cleaned_data.get('calle_referencia')
        colonia = self.cleaned_data.get('colonia')
        municipio = self.cleaned_data.get('municipio')
        observaciones = self.cleaned_data.get('observaciones')
        image = self.cleaned_data.get('image')