from django.views.generic import FormView, RedirectView, edit,ListView, DetailView, TemplateView
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate, login
from django.views.generic.list import ListView
from django.shortcuts import render, reverse
from django.contrib import messages
from IPython.display import HTML
from django.views import View
from .models import *
from .forms import *

def dashboard_page(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect (reverse('login'))
    context = {
        'reportados': Reporte.objects.all().count(),
        'pendientes': Reporte.objects.filter(status=False).count(),
        'terminados': Reporte.objects.filter(status=True).count(),

    }
    return render(request, "main/dashboard.html",context)

def UsersList(request):
    if request.method == 'POST':
        if 'addUser' in request.POST:
            form = EmployeeForm(request.POST or None)

            if form.is_valid():
                first_name = form.cleaned_data.get('first_name')
                last_name = form.cleaned_data.get('last_name')
                email = form.cleaned_data.get('email')
                password = form.cleaned_data.get('password')

                user = User.objects.create_user(
                    username=email,
                    email=email,
                    password=password,
                    first_name=first_name,
                    last_name=last_name
                )
                profile = Profile.objects.create(
                    user=user
                )
                messages.info(request, 'Usuario Creado Correctamente')
            else:
                messages.error(request, 'Error!, Completa correctamente el formulario.')

        if 'editUser' in request.POST:
            form = EmployeeEditForm(request.POST or None)

            if form.is_valid():
                id_user = form.cleaned_data.get('id_user')
                first_name = form.cleaned_data.get('first_name')
                last_name = form.cleaned_data.get('last_name')
                email = form.cleaned_data.get('email')
                password = form.cleaned_data.get('password')

                user = User.objects.get(id=id_user)

                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.username = email

                user.save()

                if password != "":
                    user.set_password(password)
                    user.save()

                messages.info(request, 'Usuario Actualizado Correctamente')

            else:
                messages.error(request, 'Error!')

        if 'deleteUser' in request.POST:
            form = EmployeeDeleteForm(request.POST or None)

            if form.is_valid():
                id_user = form.cleaned_data.get('id_user')

                user = User.objects.get(id=id_user)
                if user == request.user:
                    messages.error(request, 'Error!, No puedes eliminarte.')
                else:
                    messages.info(request, 'Usuario Eliminado Correctamente')
                    user.delete()
            else:
                messages.error(request, 'Error!')

    full_list = User.objects.all()
    
    paginator = Paginator(full_list, 50)
    page = request.GET.get('page',1)
    try:
        data_list = paginator.page(page)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    return render(request, 'main/users.html', { 'data_list': data_list })


class ProfileTemplateView(TemplateView):
    template_name = 'main/profile.html'

    def get_context_data(self, **kwargs):
        context = {
            "usr": self.request.user
            }

        p = self.request.user
        return context


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Tu password se ha actualizado correctamente!')
            return redirect("/")
        else:
            return render(request, 'auth/change_password.html', {'form': form})
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'auth/change_password.html', {
        'form': form
    })


def login_page(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect (reverse('dashboard'))

    if request.POST:

        form = LoginForm(request.POST or None)
        context = {
            "form": form
        }

        if form.is_valid():

            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")

            user = authenticate(request, username=username, password=password)

            if user is not None:
                # 10 hours per session
                request.session.set_expiry(60*60*10)
                login(request, user)

                return HttpResponseRedirect (reverse('dashboard'))
            else:
                pass
        return render(request, "auth/login.html", context)  

    return render(request, "auth/login.html")


class Logout( RedirectView ):
    pattern_name = 'bumps'

    def get( self, request, *args, **kwargs ):
        
        auth_logout( request )

        return super( Logout, self ).get( request, *args, **kwargs )

def ReportesList(request):

    full_list = Reporte.objects.all().order_by("-created")
    
    paginator = Paginator(full_list, 50)
    page = request.GET.get('page',1)
    try:
        data_list = paginator.page(page)
    except PageNotAnInteger:
        data_list = paginator.page(1)
    except EmptyPage:
        data_list = paginator.page(paginator.num_pages)

    context = {
        'data_list': data_list,
        }
    return render(request, 'main/reportes.html', context)

def reporte_page(request,folio):
    reporte = Reporte.objects.get(folio=folio)
    context = {
        'reporte': reporte,
    }
    return render(request, "main/reporte.html",context)

def change_status(request,folio):
    reporte = Reporte.objects.get(folio=folio)

    if reporte.status:
        reporte.status = False
    else:
        reporte.status = True
    reporte.save()
    
    context = {
        'reporte': reporte,
    }
    return render(request, "main/reporte.html",context)