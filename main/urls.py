from main.views import UsersList, ProfileTemplateView, login_page, Logout, change_password,ReportesList,reporte_page,change_status
from django.conf.urls import url, include
from .views import dashboard_page
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('', dashboard_page, name='dashboard'),
    url(r'^users/$', UsersList, name='users'),
    url(r'^reportes/$', ReportesList, name='reportes'),
    path('reportes/<int:folio>/', reporte_page, name='folio-reporte'),
    path('reportes/<int:folio>/change_status', change_status, name='change-status'),
    url(r'^profile/$', ProfileTemplateView.as_view(), name='profile'),
    url(r'^changepwd/$', change_password, name='changepwd'),

    url(r'^login/$', login_page, name="login"),
    url(r'^log_out/$', Logout.as_view(), name='logout'),

]
