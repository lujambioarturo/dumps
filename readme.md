## Software Engineering

#### Programa Cero Baches CDMX

Arturo Lujambio Ramírez

Magali Castillo Flores

## Development

### Django Development

#### Install Python and pip

sudo apt-get update && sudo apt-get -y upgrade

sudo apt-get install python3

sudo apt-get install -y python3-pip

pip install --upgrade pip

#### Create Virtualenv

pip3 install virtualenv

python3 -m venv bumps

#### Clone Repository

cd bumps

sudo apt install git-all

git clone git@gitlab.com:lujambioarturo/dumps.git

#### Install Django

mv bumps/ src

cd src

pip install -r requirements.txt

#### Migrations

python manage.py migrate

python manage.py makemigrations main

python manage.py migrate main

#### Statics

python manage.py collectstatic

#### Django Superuser

python manage.py createsuperuser

#### Run Django

python manage.py runserver 0.0.0.0:8000

#### Django Doc

https://www.djangoproject.com/start/
